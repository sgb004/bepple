const Client = require('ssh2').Client;

const conn = new Client();
conn.on('ready', () => {
    console.log('Client ready');
    conn.exec('ls', (err, stream) => {
        if( err ) throw err;
        stream.on('close', (code, signal) => {
            console.log('Stream :: close :: code: ' + code + ', signal: ' + signal);
            //conn.end();
        }).on('data', (data) => {
            console.log('STDOUT: ' + data);
        }).stderr.on('data', (data) => {
            console.log('STDERR: ' + data);
        });
    })
})
.connect({
    host: '192.168.1.188',
    'port': 22,
    username: 'sgb004',
    password: 'sgb'
});